#!/bin/bash
pushd engi_queue/
mkdir -p build/
cd build/
cmake ..
sudo make install
popd

pushd xyak_stdb/
mkdir -p build/
cd build/
cmake ..
sudo make install
popd

sudo python setup.py install