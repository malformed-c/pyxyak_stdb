from distutils.core import setup, Extension

module1 = Extension('XyakStdb',
                     include_dirs = ['xyak_stdb/inc', 'engi_queue/', 'engi_queue/engi_common/'],
                     libraries = ['xyak_fdb', 'engi_queue'],
                     library_dirs = ['xyak_stdb/build', 'engi_queue/build'],
                     # extra_compile_args = ['-fsanitize=address'],
                     # extra_link_args = ['-fsanitize=address'],
                     sources = ['xyak_stdb.c'])

setup (name = 'XyakStdb',
       version = '1.0',
       description = 'FlashDB python wrapper',
       ext_modules = [module1])