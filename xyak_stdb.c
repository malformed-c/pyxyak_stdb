#define PY_SSIZE_T_CLEAN
#include <Python.h>

#include <datetime.h>

#include <stdio.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <flashdb.h>

#include <unistd.h>
#include <assert.h>
#include <string.h>

#include "engi_queue.h"

#define FDB_LOG_TAG "[stdb]"

engi_queue_t queue = {0};

typedef struct data_s
{
        char name[64];
        enum
        {
            INT,
            DOUBLE,
        } type;
        union
        {
            int i;
            double d;
        } value;
        long int time;
} data_t;

static PyObject * stdb_server(PyObject *self, PyObject *args);
static PyObject * stdb_append(PyObject *self, PyObject *args);
static PyObject * stdb_query(PyObject *self, PyObject *args);

static PyMethodDef StdbMethods[] = {
    {"Server",  stdb_server, METH_VARARGS, "Start stdb."},
    {"Append",  stdb_append, METH_VARARGS, "Append TS to stdb."},
    {"Query",  stdb_query, METH_VARARGS, "Query stdb."},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

static struct PyModuleDef StdbModule = {
    PyModuleDef_HEAD_INIT,
    "XyakStdb",   /* name of module */
    NULL, /* module documentation, may be NULL */
    -1,       /* size of per-interpreter state of the module,
                 or -1 if the module keeps state in global variables. */
    StdbMethods
};

static PyObject *StdbError;

static pthread_mutex_t ts_locker;

/* TSDB object */
struct fdb_tsdb tsdb = {0};

/**/

static void lock(fdb_db_t db)
{
    pthread_mutex_lock((pthread_mutex_t *)db->user_data);
}

static void unlock(fdb_db_t db)
{
    pthread_mutex_unlock((pthread_mutex_t *)db->user_data);
}

static fdb_time_t get_time(void)
{
    return time(NULL);
}

static PyObject * stdb_server(PyObject *self, PyObject *args)
{
    fdb_err_t result;
    bool file_mode = true;
    uint32_t sec_size = 4096, db_size = sec_size * 1024 * 16;

    /* set the lock and unlock function*/
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);
    pthread_mutexattr_setkind_np(&attr, PTHREAD_MUTEX_RECURSIVE_NP);

    pthread_mutex_init(&ts_locker, &attr);
    fdb_tsdb_control(&tsdb, FDB_TSDB_CTRL_SET_LOCK, (void *)lock);
    fdb_tsdb_control(&tsdb, FDB_TSDB_CTRL_SET_UNLOCK, (void *)unlock);

    /* set the sector and database max size */
    fdb_tsdb_control(&tsdb, FDB_TSDB_CTRL_SET_SEC_SIZE, &sec_size);
    fdb_tsdb_control(&tsdb, FDB_TSDB_CTRL_SET_MAX_SIZE, &db_size);

    /* enable file mode */
    fdb_tsdb_control(&tsdb, FDB_TSDB_CTRL_SET_FILE_MODE, &file_mode);

     /* create database directory */
    mkdir("fdb_xyak_tsdb", 0777);

    result = fdb_tsdb_init(&tsdb, "log", "fdb_xyak_tsdb", get_time, 512, &ts_locker);

    /* read last saved time for simulated timestamp */
    //fdb_tsdb_control(&tsdb, FDB_TSDB_CTRL_GET_LAST_TIME, &counts);

    if (result != FDB_NO_ERR)
    {
        PyErr_SetString(StdbError, "Flash stdb server initialization failed");
        return NULL;
    }

    Py_RETURN_NONE;
}

static PyObject * stdb_append(PyObject *self, PyObject *args)
{
    struct fdb_blob blob = {0};

    data_t data = {0};

    char *name_p = 0;

    //if (!PyArg_ParseTuple(args, "si", &name_p, &data.value.i)) return NULL;
    if (!PyArg_ParseTuple(args, "sd", &name_p, &data.value.d)) return NULL;

    strcpy(data.name, name_p);

    FDB_INFO("got %s and %lf\n", data.name, data.value.d);

    fdb_err_t result = fdb_tsl_append(&tsdb, fdb_blob_make(&blob, &data, sizeof(data)));

    if (result != FDB_NO_ERR)
    {
        printf("E %d\n", result);
        PyErr_SetString(StdbError, "Flash stdb append failed");
        return NULL;
    }

    Py_RETURN_NONE;
}

static bool query_cb(fdb_tsl_t tsl, void *arg)
{
    assert(arg);
    struct 
    {
        fdb_tsdb_t tsdb;
        char name[64];
        //engi_queue_t *queue;
    } *tuple = arg;

    fdb_blob_t blob = malloc(sizeof(struct fdb_blob));
    data_t *data = malloc(sizeof(data_t));
    
    fdb_tsdb_t db = tuple->tsdb;

    fdb_blob_read((fdb_db_t)db, fdb_tsl_to_blob(tsl, fdb_blob_make(blob, data, sizeof(*data))));
    if(strcmp(data->name, tuple->name) == 0)
    {
        FDB_INFO("[query_cb] queried a TSL: time: %ld, name: %s, value: %lf\n", tsl->time, data->name, data->value.d);
        data->time = tsl->time;
        queue.enqueue(&queue, data);
    }

    return false;
}

static PyObject * stdb_query(PyObject *self, PyObject *args)
{
    char *name_p = 0;

    if (!PyArg_ParseTuple(args, "s", &name_p)) return NULL;

    struct 
    {
        fdb_tsdb_t tsdb;
        char name[64];
        //engi_queue_t *queue;
    } tuple;

    strcpy(tuple.name, name_p);

    tuple.tsdb = &tsdb;
    //tuple.queue = &queue;

    fdb_tsl_iter(&tsdb, query_cb, &tuple);

    printf("queue size %lu\n", queue.size);

    PyObject *pylist = PyList_New(queue.size);
    if(!pylist) return NULL;
    
    unsigned int q_size = queue.size;
    for(unsigned int i = 0; i < q_size; i++)
    {
        data_t *data = queue.dequeue(&queue, NULL);
        //printf("%ld %s %lf\n", data->time, data->name, data->value.d);

        PyObject *pylong = PyLong_FromLong(data->time);
        if(!pylong)
        {
            printf("E: PyLong_FromLong\n");
            return NULL;
        }

        PyObject *pytimetuple = PyTuple_Pack(2, pylong, Py_None);
        if(!pytimetuple)
        {
            printf("E: PyTuple_Pack\n");
            return NULL;
        }

        PyObject *pydatetime = PyDateTime_FromTimestamp(pytimetuple);
        if(!pydatetime)
        {
            printf("E: PyDateTime_FromTimestamp\n");
            return NULL;
        }

        PyObject *pynum = PyFloat_FromDouble(data->value.d);
        if(!pynum)
        {
            printf("E: PyFloat_FromDouble\n");
            return NULL;
        }

        PyObject *pytuple = PyTuple_Pack(2, pydatetime, pynum);
        if(!pytuple)
        {
            printf("E: PyTuple_Pack\n");
            return NULL;
        }

        PyList_SET_ITEM(pylist, i, pytuple);
        //PyList_Append(pylist, pytuple);
    }

    return pylist;
}

PyMODINIT_FUNC PyInit_XyakStdb(void)
{
    PyDateTime_IMPORT;

    PyObject *m;

    m = PyModule_Create(&StdbModule);
    if (m == NULL)
        return NULL;

    StdbError = PyErr_NewException("stdb.error", NULL, NULL);
    Py_XINCREF(StdbError);
    if (PyModule_AddObject(m, "error", StdbError) < 0) {
        Py_XDECREF(StdbError);
        Py_CLEAR(StdbError);
        Py_DECREF(m);
        return NULL;
    }

	engi_queue_init(&queue);

    return m;
}

