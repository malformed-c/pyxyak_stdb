from time import sleep
import XyakStdb
from pprint import pp
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from datetime import datetime
import random

XyakStdb.Server()

for i in range(0, 500000-120891):
    try:
        XyakStdb.Append("test", random.randint(0, 100))
    except:
        print(i)
        break

data = XyakStdb.Query("test")

date, temp = [[ i for i, j in data ],
    [ j for i, j in data ]]

plt.plot_date(date, temp, 'g')

plt.show()
